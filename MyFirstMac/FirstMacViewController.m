//
//  FirstMacViewController.m
//  MyFirstMac
//
//  Created by pp on 15/10/13.
//  Copyright © 2015年 pp. All rights reserved.
//

#import "FirstMacViewController.h"

@interface FirstMacViewController ()<NSTableViewDataSource,NSTableViewDelegate>

@end

@implementation FirstMacViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do view setup here.
    //self.leftTableView.headerView = nil;
    
    self.popButton.hidden = YES;
    self.radioButton.hidden = YES;
    [self.leftTableView reloadData];
    [self.leftTableView selectRowIndexes:[NSIndexSet indexSetWithIndex:0] byExtendingSelection:NO];
    
}


-(NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    return 4;
}

-(CGFloat)tableView:(NSTableView *)tableView heightOfRow:(NSInteger)row
{
    return 50;
}


- (NSView *)tableView:(NSTableView *)tableView viewForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    // Get a new ViewCell
    NSTableCellView *cellView = [tableView makeViewWithIdentifier:tableColumn.identifier owner:self];
    
    // Since this is a single-column table view, this would not be necessary.
    // But it's a good practice to do it in order by remember it when a table is multicolumn.
    NSLog(@"%@",tableColumn.identifier);
    if( [tableColumn.identifier isEqualToString:@"BigColumn"] )
    {
        cellView.textField.stringValue = [NSString stringWithFormat:@"第%@行",@(row)];
        return cellView;
    }
    return cellView;
}


-(void)tableViewSelectionDidChange:(NSNotification *)notification
{
    //NSLog(@"selectedRow:%@",@([self.leftTableView selectedRow]));
    if ([self.leftTableView selectedRow] == 0) {
        self.popButton.hidden = NO;
        self.radioButton.hidden = YES;
    }
    else if([self.leftTableView selectedRow] == 1)
    {
        self.popButton.hidden = YES;
        self.radioButton.hidden = NO;
    }
    else if([self.leftTableView selectedRow] == 2)
    {
        self.popButton.hidden = NO;
        self.radioButton.hidden = NO;
    }
    else
    {
        self.popButton.hidden = YES;
        self.radioButton.hidden = YES;
    }
}

@end
