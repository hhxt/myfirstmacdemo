//
//  AppDelegate.h
//  MyFirstMac
//
//  Created by pp on 15/10/12.
//  Copyright © 2015年 pp. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class FirstMacViewController;
@interface AppDelegate : NSObject <NSApplicationDelegate>


@property(strong ,nonatomic) FirstMacViewController *firstViewController;
@property (weak) IBOutlet NSImageView *ImageView;
@property (weak) IBOutlet NSPopUpButton *PopUpBtn;
@property (weak) IBOutlet NSArrayController *FirstView;
@property (weak) IBOutlet NSArrayController *SecondView;
@property (weak) IBOutlet NSArrayController *ThirdView;

@end

