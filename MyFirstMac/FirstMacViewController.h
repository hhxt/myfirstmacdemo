//
//  FirstMacViewController.h
//  MyFirstMac
//
//  Created by pp on 15/10/13.
//  Copyright © 2015年 pp. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface FirstMacViewController : NSViewController
@property (weak) IBOutlet NSTableView *leftTableView;
@property (weak) IBOutlet NSPopUpButton *popButton;
@property (weak) IBOutlet NSButton *radioButton;

@end
