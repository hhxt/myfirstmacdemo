//
//  AppDelegate.m
//  MyFirstMac
//
//  Created by pp on 15/10/12.
//  Copyright © 2015年 pp. All rights reserved.
//

#import "AppDelegate.h"
#import "FirstMacViewController.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate
- (IBAction)complete:(id)sender {
    NSLog(@"complete");
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application

    self.firstViewController = [[FirstMacViewController alloc]initWithNibName:@"FirstMacViewController" bundle:nil];
    [self.window.contentView addSubview:self.firstViewController.view];
    self.firstViewController.view.frame = ((NSView *)self.window.contentView).bounds;
    
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (IBAction)downLoad:(NSButton *)sender {
    
    NSLog(@"download");
    if (!self.ImageView.image) {
        self.ImageView.image = [NSImage imageNamed:@"06.jpg"];
    }
    else
    {
        NSLog(@"it has image");
    }
    
}
@end
